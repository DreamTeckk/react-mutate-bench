import { Provider } from 'react-redux';
import './App.css';
import Benchmark from './benchmark/Benchmark';
import { ContextImmerProvider, ContextProvider } from './benchmark/Context';
import store from './benchmark/Reducer';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <ContextImmerProvider>
          <ContextProvider>
            <Benchmark />
          </ContextProvider>
        </ContextImmerProvider>
      </Provider>
    </div>
  );
}

export default App;
