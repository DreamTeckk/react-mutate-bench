import React from 'react'
import { Bar } from 'react-chartjs-2';
import { useBenchmark } from './useBenchmark';


export default function Benchmark() {

  const { options, chartOptions, startTest, updateNumber, setUpdateNumber, actions, setAction, action } = useBenchmark()

  return (
    <div style={{
      maxHeight: '90vh',
      maxWidth: '100vw',
      margin: 'auto'
    }} >
      <button onClick={() => startTest()}>Start update</button>
      <div style={{ marginTop: '16px', marginBottom: '16px' }}>
        <label style={{ display: 'block' }} htmlFor="loop">Nombre d'update</label>
        <input onChange={(e) => setUpdateNumber(e.target.value)} value={updateNumber} type="number" name="loop" id="loop" />
        <select onChange={(e) => setAction(e.target.value)} value={action} name="action" id="action">
          {
            Object.keys(actions).map((key, idx) => (
              <option key={`action_${idx}`} value={key}>{actions[key].label}</option>
            ))
          }
        </select>
      </div>
      {
        chartOptions &&
        <Bar style={{
          margin: 'auto',
          width: '100%',
          height: '100%',
        }} options={options} data={chartOptions} />
      }
    </div >
  )
}
