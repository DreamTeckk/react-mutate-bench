import { useContext, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Context, ContextImmer } from './Context';

export const useBenchmark = () => {


  const actions = {
    COUNT: {
      actionName: 'count',
      label: 'Increment count',
    },
    NESTED_OBJECT: {
      actionName: 'nested_object',
      label: 'Update nested object',
    },
    ARRAY_PATCH: {
      actionName: 'array_patch',
      label: 'Update array entry value',
    },
  }

  const ContextState = useContext(Context);
  const ContextImmerState = useContext(ContextImmer);
  const reduxDispatch = useDispatch();

  const [datas, setDatas] = useState([0, 0, 0])
  const [updateNumber, setUpdateNumber] = useState(10000);
  const [action, setAction] = useState('COUNT')

  const [chartOptions, setChartOptions] = useState(null)

  const labels = ['Increment counter'];

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: 'Temps update state (en ms)',
      },
    },
  };


  useEffect(() => {
    const chartOptionConst = {
      labels: [actions[action]?.label],
      datasets: [
        {
          label: 'Context',
          data: [datas[0]],
          backgroundColor: '#5DD3F3',
        },
        {
          label: 'Context + Immer',
          data: [datas[1]],
          backgroundColor: '#1E8991',
        },
        {
          label: 'Redux',
          data: [datas[2]],
          backgroundColor: '#7044B4',
        },
        {
          label: 'Redux + Immer',
          data: [datas[3]],
          backgroundColor: '#04DBB9',
        },
      ],
    };
    setChartOptions({ ...chartOptionConst, labels: [actions[action]?.label,] })

  }, [action, datas])

  useEffect(() => {
    setDatas([0, 0, 0, 0])
  }, [action])

  const startTest = () => {

    const contextTime = updateContextState(actions[action].actionName);
    const contextImmerTime = updateContextImmerState(actions[action].actionName);
    const reduxTime = updateReduxState(actions[action].actionName);
    const reduxImmerTime = updateReduxImmerState(actions[action].actionName);

    setDatas([contextTime, contextImmerTime, reduxTime, reduxImmerTime])
  }

  const updateContextState = (action) => {
    const startTime = performance.now();
    for (let i = 0; i < updateNumber; i++) {
      ContextState.dispatch({ type: action });
    }
    return performance.now() - startTime

  }
  const updateContextImmerState = (action) => {
    const startTime = performance.now();
    for (let i = 0; i < updateNumber; i++) {
      ContextImmerState.dispatch({ type: `immer_${action}` });
    }
    return performance.now() - startTime
  }

  const updateReduxState = (action) => {
    const startTime = performance.now();
    for (let i = 0; i < updateNumber; i++) {
      reduxDispatch({ type: action });
    }
    return performance.now() - startTime
  }

  const updateReduxImmerState = (action) => {
    const startTime = performance.now();
    for (let i = 0; i < updateNumber; i++) {
      reduxDispatch({ type: `immer_${action}` });
    }
    return performance.now() - startTime
  }


  return {
    options,
    chartOptions,
    startTest,
    updateNumber,
    setUpdateNumber,
    labels,
    setAction,
    actions,
    action
  }
}