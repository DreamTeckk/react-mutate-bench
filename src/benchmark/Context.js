import produce from 'immer';
import { createContext, useReducer } from 'react';

export const Context = createContext({});
export const ContextImmer = createContext({});

const initialValues = {
  parent: {
    something: 1,
    anything: 'anything',
    otherthing: false,
    child: {
      something: 1,
      anything: 'anything',
      otherthing: false,
      array: [0, 1, 2, 3],
      target: 0
    }
  },
  counter: 0
}

const contextReducer = (state, { type, payload }) => {
  switch (type) {
    case 'count':
      return {
        ...state,
        counter: state.counter + 1
      }
    case 'nested_object': {
      return {
        ...state,
        parent: {
          ...state.parent,
          child: {
            ...state.parent.child,
            target: state.parent.child.target + 1
          }
        }
      }
    }
    case 'array_patch': {
      const arrayIdx = 1;
      const newArray = [...state.parent.child.array];
      newArray[arrayIdx] = newArray[arrayIdx] + 1
      return {
        ...state,
        parent: {
          ...state.parent,
          child: {
            ...state.parent.child,
            array: newArray
          }
        }
      }
    }
    default:
      return state;
  }
}

const contextImmerReducer = (state, { type, payload }) => {
  switch (type) {
    case 'immer_count':
      return produce(state, (draft) => {
        draft.counter++
      });
    case 'immer_nested_object':
      return produce(state, (draft) => {
        draft.parent.child.target++
      });
    case 'immer_array_patch': {
      return produce(state, (draft) => {
        draft.parent.child.array[1]++
      })
    }
    default:
      return state;
  }
}

export const ContextProvider = ({ children }) => {

  const [state, dispatch] = useReducer(contextReducer, initialValues)
  return (
    <Context.Provider value={{ state, dispatch }}>
      {children}
    </Context.Provider>
  )

}

export const ContextImmerProvider = ({ children }) => {

  const [state, dispatch] = useReducer(contextImmerReducer, initialValues)
  return (
    <ContextImmer.Provider value={{ state, dispatch }}>
      {children}
    </ContextImmer.Provider>
  )

}