import produce from 'immer'
import { combineReducers, createStore } from 'redux'

const initialState = {
  parent: {
    something: 1,
    anything: 'anything',
    otherthing: false,
    child: {
      something: 1,
      anything: 'anything',
      otherthing: false,
      array: [0, 1, 2, 3],
      child: 0
    }
  },
  counter: 0
}

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'count': {
      return {
        ...state,
        counter: state.counter + 1
      }
    }
    case 'nested_object': {
      return {
        ...state,
        parent: {
          ...state.parent,
          child: {
            ...state.parent.child,
            target: state.parent.child.target + 1
          }
        }
      }
    }
    case 'array_patch': {
      const arrayIdx = 1;
      const newArray = [...state.parent.child.array];
      newArray[arrayIdx] = newArray[arrayIdx] + 1
      return {
        ...state,
        parent: {
          ...state.parent,
          child: {
            ...state.parent.child,
            array: newArray
          }
        }
      }
    }
    default:
      return state
  }
}

const immer_reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'immer_count':
      return produce(state, (draft) => {
        draft.counter++
      });
    case 'immer_nested_object':
      return produce(state, (draft) => {
        draft.parent.child.target++
      });
    case 'immer_array_patch': {
      return produce(state, (draft) => {
        draft.parent.child.array[1]++
      })
    }
    default:
      return state
  }
}

const combineReducer = combineReducers({
  immer: immer_reducer,
  redux: reducer,
})

export const getCounter = (state) => state.redux.counter;
export const getImmerCounter = (state) => state.immer.counter;

const store = createStore(combineReducer);

export default store;
